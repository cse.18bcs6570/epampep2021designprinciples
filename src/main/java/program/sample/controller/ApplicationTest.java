package program.sample.controller;

import junit.framework.TestCase;

public class ApplicationTest extends TestCase {
    Calculator cal = new Calculator();

    public void testAdd() {
        assertEquals(cal.add(20, 80), 100);
    }
    public void testMultiply() {
        assertEquals(cal.mul(10, 5), 50);
    }

    public void testDivide() {
        assertEquals(cal.div(100, 5), 20.0);
    }

    public void testSubtract() {
        assertEquals(cal.sub(28, 17), 11);
    }
}